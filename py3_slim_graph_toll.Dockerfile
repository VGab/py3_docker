FROM volkg/py3_slim:devel

MAINTAINER Volker Gabler "v.gabler@tum.de"

# Add pycddlib and cvxopt with GLPK
RUN cd /tmp && \
    apk add --no-cache \
        gcc g++ make cmake file binutils ncurses \
        autoconf automake libtool sparsehash \
		expat-dev boost-dev gmp-dev mpfr-dev python3-dev && \
    \
	wget "https://github.com/CGAL/cgal/releases/download/releases%2FCGAL-4.11.3/CGAL-4.11.3.tar.xz" && \
	tar xf 	CGAL-4.11.3.tar.xz &&\
	cd CGAL-4.11.3 && \
	export PYTHON="python3" && \
	cmake . && \
	make && \
	make install && \
	cd .. && \
	wget "https://downloads.skewed.de/graph-tool/graph-tool-2.27.tar.bz2" &&\
    tar xvfj "graph-tool-2.27.tar.bz2" && \
	cd graph-tool-2.27  && \
	./autogen.sh &&\
	./configure --disable-cairo PYTHON=python3 &&\
    make && \
    make install && \
    rm -r /root/.cache && \
    apk del .build-dependencies && \
    rm -rf /tmp/*


